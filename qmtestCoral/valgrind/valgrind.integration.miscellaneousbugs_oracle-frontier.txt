==xxxxx== Memcheck, a memory error detector
==xxxxx== Copyright (C) 2002-2013, and GNU GPL'd, by Julian Seward et al.
==xxxxx== Using Valgrind-3.10.0 and LibVEX; rerun with -h for copyright info
==xxxxx== Command: test_Integration_MiscellaneousBugs oracle:frontier
==xxxxx== Parent PID: xxxxx
==xxxxx== 
--xxxxx-- 
--xxxxx-- Valgrind options:
--xxxxx--    -v
--xxxxx--    --leak-check=full
--xxxxx--    --show-reachable=yes
--xxxxx--    --error-limit=no
--xxxxx--    --log-file=/home/avalassi/CORAL/trunk/logs/qmtestCoral/valgrind/x86_64-slc6-gcc48-dbg/valgrind.integration.miscellaneousbugs_oracle-frontier.txt
--xxxxx--    --suppressions=/home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp
--xxxxx--    --gen-suppressions=all
--xxxxx--    --num-callers=50
--xxxxx--    --track-origins=yes
--xxxxx-- Contents of /proc/version:
--xxxxx--   Linux version 2.6.32-504.16.2.el6.x86_64 (mockbuild@lxsoft14.cern.ch) (gcc version 4.4.7 20120313 (Red Hat 4.4.7-11) (GCC) ) #1 SMP Tue Apr 21 21:44:51 CEST 2015
--xxxxx-- Arch and hwcaps: AMD64, LittleEndian, amd64-cx16-sse3
--xxxxx-- Page sizes: currently 4096, max supported 4096
--xxxxx-- Valgrind library directory: /cvmfs/sft.cern.ch/lcg/releases/LCG_78root6/valgrind/3.10.0/x86_64-slc6-gcc48-dbg/lib/valgrind
--xxxxx-- Reading syms from /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/tests/bin/test_Integration_MiscellaneousBugs
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/valgrind/3.10.0-073ad/x86_64-slc6-gcc48-dbg/lib/valgrind/memcheck-amd64-linux
--xxxxx--    object doesn't have a dynamic symbol table
--xxxxx-- Reading syms from /lib64/ld-2.12.so
--xxxxx-- Scheduler: using generic scheduler lock implementation.
--xxxxx-- Reading suppressions file: /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp
--xxxxx-- Reading suppressions file: /cvmfs/sft.cern.ch/lcg/releases/LCG_78root6/valgrind/3.10.0/x86_64-slc6-gcc48-dbg/lib/valgrind/default.supp
==xxxxx== embedded gdbserver: reading from /tmp/vgdb-pipe-from-vgdb-to-xxxxx-by-avalassi-on-aicoral61.cern.ch
==xxxxx== embedded gdbserver: writing to   /tmp/vgdb-pipe-to-vgdb-from-xxxxx-by-avalassi-on-aicoral61.cern.ch
==xxxxx== embedded gdbserver: shared mem   /tmp/vgdb-pipe-shared-mem-vgdb-xxxxx-by-avalassi-on-aicoral61.cern.ch
==xxxxx== 
==xxxxx== TO CONTROL THIS PROCESS USING vgdb (which you probably
==xxxxx== don't want to do, unless you know exactly what you're doing,
==xxxxx== or are doing some strange experiment):
==xxxxx==   /cvmfs/sft.cern.ch/lcg/releases/LCG_78root6/valgrind/3.10.0/x86_64-slc6-gcc48-dbg/lib/valgrind/../../bin/vgdb --pid=xxxxx ...command...
==xxxxx== 
==xxxxx== TO DEBUG THIS PROCESS USING GDB: start GDB like this
==xxxxx==   /path/to/gdb test_Integration_MiscellaneousBugs
==xxxxx== and then give GDB the following command
==xxxxx==   target remote | /cvmfs/sft.cern.ch/lcg/releases/LCG_78root6/valgrind/3.10.0/x86_64-slc6-gcc48-dbg/lib/valgrind/../../bin/vgdb --pid=xxxxx
==xxxxx== --pid is optional if only one valgrind process is running
==xxxxx== 
--xxxxx-- REDIR: 0x3b5bc17c30 (ld-linux-x86-64.so.2:strlen) redirected to 0x3806bd71 (vgPlain_amd64_linux_REDIR_FOR_strlen)
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/valgrind/3.10.0-073ad/x86_64-slc6-gcc48-dbg/lib/valgrind/vgpreload_core-amd64-linux.so
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/valgrind/3.10.0-073ad/x86_64-slc6-gcc48-dbg/lib/valgrind/vgpreload_memcheck-amd64-linux.so
==xxxxx== WARNING: new redirection conflicts with existing -- ignoring it
--xxxxx--     old: 0x3b5bc17c30 (strlen              ) R-> (0000.0) 0x3806bd71 vgPlain_amd64_linux_REDIR_FOR_strlen
--xxxxx--     new: 0x3b5bc17c30 (strlen              ) R-> (2007.0) 0x04a09b30 strlen
--xxxxx-- REDIR: 0x3b5bc17a40 (ld-linux-x86-64.so.2:index) redirected to 0x4a096e0 (index)
--xxxxx-- REDIR: 0x3b5bc17ac0 (ld-linux-x86-64.so.2:strcmp) redirected to 0x4a0ac80 (strcmp)
--xxxxx-- REDIR: 0x3b5bc18a10 (ld-linux-x86-64.so.2:mempcpy) redirected to 0x4a0d730 (mempcpy)
--xxxxx-- Reading syms from /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/tests/lib/libtest_TestEnv.so
--xxxxx-- Reading syms from /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/lib/liblcg_CoralCommon.so
--xxxxx-- Reading syms from /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/lib/liblcg_RelationalAccess.so
--xxxxx-- Reading syms from /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/lib/liblcg_CoralKernel.so
--xxxxx-- Reading syms from /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/lib/liblcg_CoralBase.so
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/Boost/1.55.0_python2.7-dcbb8/x86_64-slc6-gcc48-dbg/lib/libboost_thread-gcc48-mt-d-1_55.so.1.55.0
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/Boost/1.55.0_python2.7-dcbb8/x86_64-slc6-gcc48-dbg/lib/libboost_system-gcc48-mt-d-1_55.so.1.55.0
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/Boost/1.55.0_python2.7-dcbb8/x86_64-slc6-gcc48-dbg/lib/libboost_filesystem-gcc48-mt-d-1_55.so.1.55.0
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/CppUnit/1.12.1_p1-31a6a/x86_64-slc6-gcc48-dbg/lib/libcppunit-1.12.so.1.0.0
--xxxxx-- Reading syms from /lib64/libdl-2.12.so
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/Boost/1.55.0_python2.7-dcbb8/x86_64-slc6-gcc48-dbg/lib/libboost_date_time-gcc48-mt-d-1_55.so.1.55.0
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/gcc/4.8.4/x86_64-slc6/lib64/libstdc++.so.6.0.19
--xxxxx-- Reading syms from /lib64/libm-2.12.so
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/gcc/4.8.4/x86_64-slc6/lib64/libgcc_s.so.1
--xxxxx-- Reading syms from /lib64/libpthread-2.12.so
--xxxxx-- Reading syms from /lib64/libc-2.12.so
--xxxxx-- Reading syms from /lib64/librt-2.12.so
--xxxxx-- REDIR: 0x3b5c084d70 (libc.so.6:strcasecmp) redirected to 0x4801696 (_vgnU_ifunc_wrapper)
--xxxxx-- REDIR: 0x3b5c087030 (libc.so.6:strncasecmp) redirected to 0x4801696 (_vgnU_ifunc_wrapper)
--xxxxx-- REDIR: 0x3b5c081200 (libc.so.6:__GI_strlen) redirected to 0x4a09a90 (__GI_strlen)
--xxxxx-- REDIR: 0x3b5c082ce0 (libc.so.6:__GI_strrchr) redirected to 0x4a093f0 (__GI_strrchr)
--xxxxx-- REDIR: 0x3b5c07f780 (libc.so.6:strcmp) redirected to 0x4801696 (_vgnU_ifunc_wrapper)
--xxxxx-- REDIR: 0x3b5c128450 (libc.so.6:__strcmp_sse42) redirected to 0x4a0ac30 (__strcmp_sse42)
--xxxxx-- REDIR: 0x62ea2b0 (libstdc++.so.6:operator new(unsigned long)) redirected to 0x4a06f4e (operator new(unsigned long))
--xxxxx-- REDIR: 0x3b5c089710 (libc.so.6:memcpy) redirected to 0x4a0b330 (memcpy)
--xxxxx-- REDIR: 0x3b5c0811c0 (libc.so.6:strlen) redirected to 0x4801696 (_vgnU_ifunc_wrapper)
--xxxxx-- REDIR: 0x3b5c133720 (libc.so.6:__strlen_sse42) redirected to 0x4a09af0 (__strlen_sse42)
--xxxxx-- REDIR: 0x3b5c07a320 (libc.so.6:calloc) redirected to 0x4a087ae (calloc)
--xxxxx-- REDIR: 0x3b5c081430 (libc.so.6:__GI_strncmp) redirected to 0x4a0a2c0 (__GI_strncmp)
--xxxxx-- REDIR: 0x3b5c083470 (libc.so.6:bcmp) redirected to 0x4801696 (_vgnU_ifunc_wrapper)
--xxxxx-- REDIR: 0x3b5c13e7d0 (libc.so.6:__memcmp_sse4_1) redirected to 0x4a0c590 (__memcmp_sse4_1)
--xxxxx-- REDIR: 0x62e8570 (libstdc++.so.6:operator delete(void*)) redirected to 0x4a07f7a (operator delete(void*))
--xxxxx-- REDIR: 0x3b5c07a6e0 (libc.so.6:malloc) redirected to 0x4a06a46 (malloc)
--xxxxx-- Reading syms from /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/lib/liblcg_ConnectionService.so
--xxxxx-- REDIR: 0x3b5c07b5c0 (libc.so.6:free) redirected to 0x4a07b60 (free)
--xxxxx-- REDIR: 0x3b5c08ea20 (libc.so.6:__GI_strstr) redirected to 0x4a0d9c0 (__strstr_sse2)
--xxxxx-- REDIR: 0x3b5c080c40 (libc.so.6:__GI_strcpy) redirected to 0x4a09c30 (__GI_strcpy)
--xxxxx-- REDIR: 0x3b5c07f7c0 (libc.so.6:__GI_strcmp) redirected to 0x4a0ab90 (__GI_strcmp)
--xxxxx-- Reading syms from /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/lib/liblcg_RelationalService.so
--xxxxx-- REDIR: 0x3b5bc18b60 (ld-linux-x86-64.so.2:stpcpy) redirected to 0x4a0cab0 (stpcpy)
--xxxxx-- Reading syms from /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/lib/liblcg_XMLLookupService.so
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/XercesC/3.1.1p1-8ccd5/x86_64-slc6-gcc48-dbg/lib/libxerces-c-3.1.so
--xxxxx-- Reading syms from /lib64/libnsl-2.12.so
--xxxxx-- REDIR: 0x3b5c0838a0 (libc.so.6:memmove) redirected to 0x4a0cd90 (memmove)
--xxxxx-- REDIR: 0x3b5c0845a0 (libc.so.6:mempcpy) redirected to 0x4a0d460 (mempcpy)
--xxxxx-- REDIR: 0x3b5c07f6d0 (libc.so.6:index) redirected to 0x4801696 (_vgnU_ifunc_wrapper)
--xxxxx-- REDIR: 0x3b5c1283a0 (libc.so.6:__strchr_sse42) redirected to 0x4a095e0 (index)
--xxxxx-- REDIR: 0x3b5c07f700 (libc.so.6:__GI_strchr) redirected to 0x4a09520 (__GI_strchr)
--xxxxx-- Reading syms from /usr/lib64/gconv/UTF-16.so
--xxxxx-- REDIR: 0x3b5c12b4f0 (libc.so.6:__strcasecmp_sse42) redirected to 0x4a0a410 (strcasecmp)
--xxxxx-- REDIR: 0x3b5c083a40 (libc.so.6:memset) redirected to 0x4801696 (_vgnU_ifunc_wrapper)
--xxxxx-- REDIR: 0x3b5c083a80 (libc.so.6:__GI_memset) redirected to 0x4a0cce0 (memset)
--xxxxx-- REDIR: 0x3b5c0813f0 (libc.so.6:strncmp) redirected to 0x4801696 (_vgnU_ifunc_wrapper)
--xxxxx-- REDIR: 0x3b5c1292f0 (libc.so.6:__strncmp_sse42) redirected to 0x4a0a3a0 (__strncmp_sse42)
--xxxxx-- Reading syms from /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/lib/liblcg_OracleAccess.so
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libnnz11.so
--xxxxx-- Reading syms from /lib64/libaio.so.1.0.1
--xxxxx--    object doesn't have a symbol table
--xxxxx-- Reading syms from /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/lib/liblcg_XMLAuthenticationService.so
--xxxxx-- REDIR: 0x3b5c082c80 (libc.so.6:strncpy) redirected to 0x4801696 (_vgnU_ifunc_wrapper)
--xxxxx-- REDIR: 0x3b5c134be0 (libc.so.6:???) redirected to 0x4a09d10 (strncpy)
--xxxxx-- REDIR: 0x3b5c07bab0 (libc.so.6:realloc) redirected to 0x4a08950 (realloc)
--xxxxx-- REDIR: 0x3b5c08ab90 (libc.so.6:strchrnul) redirected to 0x4a0d340 (strchrnul)
--xxxxx-- REDIR: 0x3b5c081350 (libc.so.6:strncat) redirected to 0x4a09900 (strncat)
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libociicus.so
--xxxxx-- REDIR: 0x3b5c0350e0 (libc.so.6:putenv) redirected to 0x4a0deb0 (putenv)
--xxxxx-- REDIR: 0x3b5c0812e0 (libc.so.6:strnlen) redirected to 0x4a09a10 (strnlen)
--xxxxx-- REDIR: 0x3b5c08e560 (libc.so.6:strstr) redirected to 0x4801696 (_vgnU_ifunc_wrapper)
--xxxxx-- REDIR: 0x3b5c12a650 (libc.so.6:__strstr_sse42) redirected to 0x4a0da50 (__strstr_sse42)
--xxxxx-- Reading syms from /usr/lib64/libnuma.so.1
--xxxxx--    object doesn't have a symbol table
--xxxxx-- REDIR: 0x3b5c0833f0 (libc.so.6:memchr) redirected to 0x4a0ad20 (memchr)
--xxxxx-- REDIR: 0x3b5c08eeb0 (libc.so.6:strcasestr) redirected to 0x4801696 (_vgnU_ifunc_wrapper)
--xxxxx-- REDIR: 0x3b5c12f240 (libc.so.6:__strcasestr_sse42) redirected to 0x4a0dc20 (strcasestr)
--xxxxx-- REDIR: 0x3b5c082cb0 (libc.so.6:rindex) redirected to 0x4801696 (_vgnU_ifunc_wrapper)
--xxxxx-- REDIR: 0x3b5c12a4b0 (libc.so.6:__strrchr_sse42) redirected to 0x4a09480 (__strrchr_sse42)
--xxxxx-- REDIR: 0x3b5c08ab40 (libc.so.6:__GI___rawmemchr) redirected to 0x4a0d3a0 (__GI___rawmemchr)
--xxxxx-- REDIR: 0x3b5c12d190 (libc.so.6:__strncasecmp_sse42) redirected to 0x4a0a4f0 (strncasecmp)
--xxxxx-- REDIR: 0xffffffffff600000 (???:???) redirected to 0x3806bd53 (vgPlain_amd64_linux_REDIR_FOR_vgettimeofday)
--xxxxx-- REDIR: 0x3b5c084c20 (libc.so.6:__GI_stpcpy) redirected to 0x4a0c730 (__GI_stpcpy)
--xxxxx-- Reading syms from /lib64/libnss_files-2.12.so
--xxxxx-- Reading syms from /lib64/libnss_sss.so.2
--xxxxx--    object doesn't have a symbol table
--xxxxx-- REDIR: 0xffffffffff600400 (???:???) redirected to 0x3806bd5d (vgPlain_amd64_linux_REDIR_FOR_vtime)
--xxxxx-- REDIR: 0x3b5c08de80 (libc.so.6:__GI_strncpy) redirected to 0x4a09e60 (__GI_strncpy)
--xxxxx-- REDIR: 0xb10ccb0 (NONE:_intel_fast_memcpy) redirected to 0x4a0c1e0 (_intel_fast_memcpy)
--xxxxx-- REDIR: 0x3b5c035770 (libc.so.6:setenv) redirected to 0x4a0dff0 (setenv)
--xxxxx-- REDIR: 0x62ea3c0 (libstdc++.so.6:operator new[](unsigned long)) redirected to 0x4a075f3 (operator new[](unsigned long))
--xxxxx-- REDIR: 0x62e85a0 (libstdc++.so.6:operator delete[](void*)) redirected to 0x4a0842a (operator delete[](void*))
--xxxxx-- Reading syms from /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/lib/liblcg_FrontierAccess.so
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/frontier_client/2.8.10-e6888/x86_64-slc6-gcc48-dbg/lib/libfrontier_client.so.2
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/expat/2.0.1-6c8ce/x86_64-slc6-gcc48-dbg/lib/libexpat.so.1.5.2
--xxxxx-- Reading syms from /usr/lib64/libssl.so.1.0.1e
--xxxxx--    object doesn't have a symbol table
--xxxxx-- Reading syms from /usr/lib64/libcrypto.so.1.0.1e
--xxxxx--    object doesn't have a symbol table
--xxxxx-- Reading syms from /lib64/libz.so.1.2.3
--xxxxx--    object doesn't have a symbol table
--xxxxx-- Reading syms from /lib64/libgssapi_krb5.so.2.2
--xxxxx--    object doesn't have a symbol table
--xxxxx-- Reading syms from /lib64/libkrb5.so.3.3
--xxxxx--    object doesn't have a symbol table
--xxxxx-- Reading syms from /lib64/libcom_err.so.2.1
--xxxxx--    object doesn't have a symbol table
--xxxxx-- Reading syms from /lib64/libk5crypto.so.3.1
--xxxxx--    object doesn't have a symbol table
--xxxxx-- Reading syms from /lib64/libkrb5support.so.0.1
--xxxxx--    object doesn't have a symbol table
--xxxxx-- Reading syms from /lib64/libkeyutils.so.1.3
--xxxxx--    object doesn't have a symbol table
--xxxxx-- Reading syms from /lib64/libresolv-2.12.so
--xxxxx-- Reading syms from /lib64/libselinux.so.1
--xxxxx--    object doesn't have a symbol table
--xxxxx-- REDIR: 0x3b5c084a00 (libc.so.6:bcopy) redirected to 0x4a0d250 (bcopy)
==xxxxx== Invalid read of size 8
==xxxxx==    at 0xD44AD0B: frontier::Session::clean() (frontier-cpp.cc:400)
==xxxxx==    by 0xD443A71: frontier::AnyData::clean() (anydata.cc:160)
==xxxxx==    by 0xD20AB3B: coral::FrontierAccess::Statement::reset() (Statement.cpp:474)
==xxxxx==    by 0xD208CB7: coral::FrontierAccess::Statement::~Statement() (Statement.cpp:177)
==xxxxx==    by 0xD1EA198: coral::FrontierAccess::Cursor::close() (Cursor.cpp:41)
==xxxxx==    by 0xD1EA083: coral::FrontierAccess::Cursor::~Cursor() (Cursor.cpp:14)
==xxxxx==    by 0xD1EA0D1: coral::FrontierAccess::Cursor::~Cursor() (Cursor.cpp:15)
==xxxxx==    by 0xD1D5671: coral::FrontierAccess::Query::~Query() (Query.cpp:38)
==xxxxx==    by 0xD1D5723: coral::FrontierAccess::Query::~Query() (Query.cpp:39)
==xxxxx==    by 0x4196B4: std::auto_ptr<coral::IQuery>::~auto_ptr() (in /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/tests/bin/test_Integration_MiscellaneousBugs)
==xxxxx==    by 0x4134D6: coral::CoralMiscellaneousBugsTest::_test_reservedWord(std::string) (test_MiscellaneousBugs.cpp:378)
==xxxxx==    by 0x41231C: coral::CoralMiscellaneousBugsTest::test_bug91075_SYSTIMESTAMP() (in /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/tests/bin/test_Integration_MiscellaneousBugs)
==xxxxx==    by 0x41B3A1: CppUnit::TestCaller<coral::CoralMiscellaneousBugsTest>::runTest() (TestCaller.h:166)
==xxxxx==    by 0x5E3CAA1: CppUnit::TestCaseMethodFunctor::operator()() const (TestCase.cpp:32)
==xxxxx==    by 0x5E3326F: CppUnit::DefaultProtector::protect(CppUnit::Functor const&, CppUnit::ProtectorContext const&) (DefaultProtector.cpp:15)
==xxxxx==    by 0x5E39DEC: CppUnit::ProtectorChain::protect(CppUnit::Functor const&, CppUnit::ProtectorContext const&) (ProtectorChain.cpp:77)
==xxxxx==    by 0x5E42659: CppUnit::TestResult::protect(CppUnit::Functor const&, CppUnit::Test*, std::string const&) (TestResult.cpp:178)
==xxxxx==    by 0x5E3C7A9: CppUnit::TestCase::run(CppUnit::TestResult*) (TestCase.cpp:92)
==xxxxx==    by 0x5E3CE52: CppUnit::TestComposite::doRunChildTests(CppUnit::TestResult*) (TestComposite.cpp:64)
==xxxxx==    by 0x5E3CD6D: CppUnit::TestComposite::run(CppUnit::TestResult*) (TestComposite.cpp:23)
==xxxxx==    by 0x5E3CE52: CppUnit::TestComposite::doRunChildTests(CppUnit::TestResult*) (TestComposite.cpp:64)
==xxxxx==    by 0x5E3CD6D: CppUnit::TestComposite::run(CppUnit::TestResult*) (TestComposite.cpp:23)
==xxxxx==    by 0x5E42391: CppUnit::TestResult::runTest(CppUnit::Test*) (TestResult.cpp:145)
==xxxxx==    by 0x5E4464D: CppUnit::TestRunner::run(CppUnit::TestResult&, std::string const&) (TestRunner.cpp:96)
==xxxxx==    by 0x5E465BB: CppUnit::TextTestRunner::run(std::string, bool, bool, bool) (TextTestRunner.cpp:64)
==xxxxx==    by 0x40B659: coral::CoralCppUnitTest::Main() (CoralCppUnitTest.h:171)
==xxxxx==    by 0x40AAC9: main (test_MiscellaneousBugs.cpp:744)
==xxxxx==  Address 0xda42b48 is 24 bytes inside a block of size 40 free'd
==xxxxx==    at 0x4A08001: operator delete(void*) (vg_replace_malloc.c:507)
==xxxxx==    by 0xD20AACA: coral::FrontierAccess::Statement::reset() (Statement.cpp:464)
==xxxxx==    by 0xD208CB7: coral::FrontierAccess::Statement::~Statement() (Statement.cpp:177)
==xxxxx==    by 0xD1EA198: coral::FrontierAccess::Cursor::close() (Cursor.cpp:41)
==xxxxx==    by 0xD1EA083: coral::FrontierAccess::Cursor::~Cursor() (Cursor.cpp:14)
==xxxxx==    by 0xD1EA0D1: coral::FrontierAccess::Cursor::~Cursor() (Cursor.cpp:15)
==xxxxx==    by 0xD1D5671: coral::FrontierAccess::Query::~Query() (Query.cpp:38)
==xxxxx==    by 0xD1D5723: coral::FrontierAccess::Query::~Query() (Query.cpp:39)
==xxxxx==    by 0x4196B4: std::auto_ptr<coral::IQuery>::~auto_ptr() (in /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/tests/bin/test_Integration_MiscellaneousBugs)
==xxxxx==    by 0x4134D6: coral::CoralMiscellaneousBugsTest::_test_reservedWord(std::string) (test_MiscellaneousBugs.cpp:378)
==xxxxx==    by 0x41231C: coral::CoralMiscellaneousBugsTest::test_bug91075_SYSTIMESTAMP() (in /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/tests/bin/test_Integration_MiscellaneousBugs)
==xxxxx==    by 0x41B3A1: CppUnit::TestCaller<coral::CoralMiscellaneousBugsTest>::runTest() (TestCaller.h:166)
==xxxxx==    by 0x5E3CAA1: CppUnit::TestCaseMethodFunctor::operator()() const (TestCase.cpp:32)
==xxxxx==    by 0x5E3326F: CppUnit::DefaultProtector::protect(CppUnit::Functor const&, CppUnit::ProtectorContext const&) (DefaultProtector.cpp:15)
==xxxxx==    by 0x5E39DEC: CppUnit::ProtectorChain::protect(CppUnit::Functor const&, CppUnit::ProtectorContext const&) (ProtectorChain.cpp:77)
==xxxxx==    by 0x5E42659: CppUnit::TestResult::protect(CppUnit::Functor const&, CppUnit::Test*, std::string const&) (TestResult.cpp:178)
==xxxxx==    by 0x5E3C7A9: CppUnit::TestCase::run(CppUnit::TestResult*) (TestCase.cpp:92)
==xxxxx==    by 0x5E3CE52: CppUnit::TestComposite::doRunChildTests(CppUnit::TestResult*) (TestComposite.cpp:64)
==xxxxx==    by 0x5E3CD6D: CppUnit::TestComposite::run(CppUnit::TestResult*) (TestComposite.cpp:23)
==xxxxx==    by 0x5E3CE52: CppUnit::TestComposite::doRunChildTests(CppUnit::TestResult*) (TestComposite.cpp:64)
==xxxxx==    by 0x5E3CD6D: CppUnit::TestComposite::run(CppUnit::TestResult*) (TestComposite.cpp:23)
==xxxxx==    by 0x5E42391: CppUnit::TestResult::runTest(CppUnit::Test*) (TestResult.cpp:145)
==xxxxx==    by 0x5E4464D: CppUnit::TestRunner::run(CppUnit::TestResult&, std::string const&) (TestRunner.cpp:96)
==xxxxx==    by 0x5E465BB: CppUnit::TextTestRunner::run(std::string, bool, bool, bool) (TextTestRunner.cpp:64)
==xxxxx==    by 0x40B659: coral::CoralCppUnitTest::Main() (CoralCppUnitTest.h:171)
==xxxxx==    by 0x40AAC9: main (test_MiscellaneousBugs.cpp:744)
==xxxxx== 
{
   <insert_a_suppression_name_here>
   Memcheck:Addr8
   fun:_ZN8frontier7Session5cleanEv
   fun:_ZN8frontier7AnyData5cleanEv
   fun:_ZN5coral14FrontierAccess9Statement5resetEv
   fun:_ZN5coral14FrontierAccess9StatementD1Ev
   fun:_ZN5coral14FrontierAccess6Cursor5closeEv
   fun:_ZN5coral14FrontierAccess6CursorD1Ev
   fun:_ZN5coral14FrontierAccess6CursorD0Ev
   fun:_ZN5coral14FrontierAccess5QueryD1Ev
   fun:_ZN5coral14FrontierAccess5QueryD0Ev
   fun:_ZNSt8auto_ptrIN5coral6IQueryEED1Ev
   fun:_ZN5coral26CoralMiscellaneousBugsTest18_test_reservedWordESs
   fun:_ZN5coral26CoralMiscellaneousBugsTest26test_bug91075_SYSTIMESTAMPEv
   fun:_ZN7CppUnit10TestCallerIN5coral26CoralMiscellaneousBugsTestEE7runTestEv
   fun:_ZNK7CppUnit21TestCaseMethodFunctorclEv
   fun:_ZN7CppUnit16DefaultProtector7protectERKNS_7FunctorERKNS_16ProtectorContextE
   fun:_ZN7CppUnit14ProtectorChain7protectERKNS_7FunctorERKNS_16ProtectorContextE
   fun:_ZN7CppUnit10TestResult7protectERKNS_7FunctorEPNS_4TestERKSs
   fun:_ZN7CppUnit8TestCase3runEPNS_10TestResultE
   fun:_ZN7CppUnit13TestComposite15doRunChildTestsEPNS_10TestResultE
   fun:_ZN7CppUnit13TestComposite3runEPNS_10TestResultE
   fun:_ZN7CppUnit13TestComposite15doRunChildTestsEPNS_10TestResultE
   fun:_ZN7CppUnit13TestComposite3runEPNS_10TestResultE
   fun:_ZN7CppUnit10TestResult7runTestEPNS_4TestE
   fun:_ZN7CppUnit10TestRunner3runERNS_10TestResultERKSs
}
--xxxxx-- REDIR: 0x3b5c0351f0 (libc.so.6:unsetenv) redirected to 0x4a0df50 (unsetenv)
--xxxxx-- Discarding syms at 0x824d580-0x824ecd8 in /usr/lib64/gconv/UTF-16.so due to munmap()
--xxxxx-- Discarding syms at 0xc5161f0-0xc51e648 in /lib64/libnss_files-2.12.so due to munmap()
--xxxxx-- Discarding syms at 0xc723730-0xc728278 in /lib64/libnss_sss.so.2 due to munmap()
==xxxxx== 
==xxxxx== HEAP SUMMARY:
==xxxxx==     in use at exit: 1,127,693 bytes in 6,054 blocks
==xxxxx==   total heap usage: 34,779 allocs, 28,725 frees, 8,246,171 bytes allocated
==xxxxx== 
==xxxxx== Searching for pointers to 6,054 not-freed blocks
==xxxxx== Checked 4,159,880 bytes
==xxxxx== 
==xxxxx== LEAK SUMMARY:
==xxxxx==    definitely lost: 0 bytes in 0 blocks
==xxxxx==    indirectly lost: 0 bytes in 0 blocks
==xxxxx==      possibly lost: 0 bytes in 0 blocks
==xxxxx==    still reachable: 0 bytes in 0 blocks
==xxxxx==         suppressed: 1,127,693 bytes in 6,054 blocks
==xxxxx== 
==xxxxx== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 18418 from 503)
==xxxxx== 
==xxxxx== 1 errors in context 1 of 1:
==xxxxx== Invalid read of size 8
==xxxxx==    at 0xD44AD0B: frontier::Session::clean() (frontier-cpp.cc:400)
==xxxxx==    by 0xD443A71: frontier::AnyData::clean() (anydata.cc:160)
==xxxxx==    by 0xD20AB3B: coral::FrontierAccess::Statement::reset() (Statement.cpp:474)
==xxxxx==    by 0xD208CB7: coral::FrontierAccess::Statement::~Statement() (Statement.cpp:177)
==xxxxx==    by 0xD1EA198: coral::FrontierAccess::Cursor::close() (Cursor.cpp:41)
==xxxxx==    by 0xD1EA083: coral::FrontierAccess::Cursor::~Cursor() (Cursor.cpp:14)
==xxxxx==    by 0xD1EA0D1: coral::FrontierAccess::Cursor::~Cursor() (Cursor.cpp:15)
==xxxxx==    by 0xD1D5671: coral::FrontierAccess::Query::~Query() (Query.cpp:38)
==xxxxx==    by 0xD1D5723: coral::FrontierAccess::Query::~Query() (Query.cpp:39)
==xxxxx==    by 0x4196B4: std::auto_ptr<coral::IQuery>::~auto_ptr() (in /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/tests/bin/test_Integration_MiscellaneousBugs)
==xxxxx==    by 0x4134D6: coral::CoralMiscellaneousBugsTest::_test_reservedWord(std::string) (test_MiscellaneousBugs.cpp:378)
==xxxxx==    by 0x41231C: coral::CoralMiscellaneousBugsTest::test_bug91075_SYSTIMESTAMP() (in /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/tests/bin/test_Integration_MiscellaneousBugs)
==xxxxx==    by 0x41B3A1: CppUnit::TestCaller<coral::CoralMiscellaneousBugsTest>::runTest() (TestCaller.h:166)
==xxxxx==    by 0x5E3CAA1: CppUnit::TestCaseMethodFunctor::operator()() const (TestCase.cpp:32)
==xxxxx==    by 0x5E3326F: CppUnit::DefaultProtector::protect(CppUnit::Functor const&, CppUnit::ProtectorContext const&) (DefaultProtector.cpp:15)
==xxxxx==    by 0x5E39DEC: CppUnit::ProtectorChain::protect(CppUnit::Functor const&, CppUnit::ProtectorContext const&) (ProtectorChain.cpp:77)
==xxxxx==    by 0x5E42659: CppUnit::TestResult::protect(CppUnit::Functor const&, CppUnit::Test*, std::string const&) (TestResult.cpp:178)
==xxxxx==    by 0x5E3C7A9: CppUnit::TestCase::run(CppUnit::TestResult*) (TestCase.cpp:92)
==xxxxx==    by 0x5E3CE52: CppUnit::TestComposite::doRunChildTests(CppUnit::TestResult*) (TestComposite.cpp:64)
==xxxxx==    by 0x5E3CD6D: CppUnit::TestComposite::run(CppUnit::TestResult*) (TestComposite.cpp:23)
==xxxxx==    by 0x5E3CE52: CppUnit::TestComposite::doRunChildTests(CppUnit::TestResult*) (TestComposite.cpp:64)
==xxxxx==    by 0x5E3CD6D: CppUnit::TestComposite::run(CppUnit::TestResult*) (TestComposite.cpp:23)
==xxxxx==    by 0x5E42391: CppUnit::TestResult::runTest(CppUnit::Test*) (TestResult.cpp:145)
==xxxxx==    by 0x5E4464D: CppUnit::TestRunner::run(CppUnit::TestResult&, std::string const&) (TestRunner.cpp:96)
==xxxxx==    by 0x5E465BB: CppUnit::TextTestRunner::run(std::string, bool, bool, bool) (TextTestRunner.cpp:64)
==xxxxx==    by 0x40B659: coral::CoralCppUnitTest::Main() (CoralCppUnitTest.h:171)
==xxxxx==    by 0x40AAC9: main (test_MiscellaneousBugs.cpp:744)
==xxxxx==  Address 0xda42b48 is 24 bytes inside a block of size 40 free'd
==xxxxx==    at 0x4A08001: operator delete(void*) (vg_replace_malloc.c:507)
==xxxxx==    by 0xD20AACA: coral::FrontierAccess::Statement::reset() (Statement.cpp:464)
==xxxxx==    by 0xD208CB7: coral::FrontierAccess::Statement::~Statement() (Statement.cpp:177)
==xxxxx==    by 0xD1EA198: coral::FrontierAccess::Cursor::close() (Cursor.cpp:41)
==xxxxx==    by 0xD1EA083: coral::FrontierAccess::Cursor::~Cursor() (Cursor.cpp:14)
==xxxxx==    by 0xD1EA0D1: coral::FrontierAccess::Cursor::~Cursor() (Cursor.cpp:15)
==xxxxx==    by 0xD1D5671: coral::FrontierAccess::Query::~Query() (Query.cpp:38)
==xxxxx==    by 0xD1D5723: coral::FrontierAccess::Query::~Query() (Query.cpp:39)
==xxxxx==    by 0x4196B4: std::auto_ptr<coral::IQuery>::~auto_ptr() (in /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/tests/bin/test_Integration_MiscellaneousBugs)
==xxxxx==    by 0x4134D6: coral::CoralMiscellaneousBugsTest::_test_reservedWord(std::string) (test_MiscellaneousBugs.cpp:378)
==xxxxx==    by 0x41231C: coral::CoralMiscellaneousBugsTest::test_bug91075_SYSTIMESTAMP() (in /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/tests/bin/test_Integration_MiscellaneousBugs)
==xxxxx==    by 0x41B3A1: CppUnit::TestCaller<coral::CoralMiscellaneousBugsTest>::runTest() (TestCaller.h:166)
==xxxxx==    by 0x5E3CAA1: CppUnit::TestCaseMethodFunctor::operator()() const (TestCase.cpp:32)
==xxxxx==    by 0x5E3326F: CppUnit::DefaultProtector::protect(CppUnit::Functor const&, CppUnit::ProtectorContext const&) (DefaultProtector.cpp:15)
==xxxxx==    by 0x5E39DEC: CppUnit::ProtectorChain::protect(CppUnit::Functor const&, CppUnit::ProtectorContext const&) (ProtectorChain.cpp:77)
==xxxxx==    by 0x5E42659: CppUnit::TestResult::protect(CppUnit::Functor const&, CppUnit::Test*, std::string const&) (TestResult.cpp:178)
==xxxxx==    by 0x5E3C7A9: CppUnit::TestCase::run(CppUnit::TestResult*) (TestCase.cpp:92)
==xxxxx==    by 0x5E3CE52: CppUnit::TestComposite::doRunChildTests(CppUnit::TestResult*) (TestComposite.cpp:64)
==xxxxx==    by 0x5E3CD6D: CppUnit::TestComposite::run(CppUnit::TestResult*) (TestComposite.cpp:23)
==xxxxx==    by 0x5E3CE52: CppUnit::TestComposite::doRunChildTests(CppUnit::TestResult*) (TestComposite.cpp:64)
==xxxxx==    by 0x5E3CD6D: CppUnit::TestComposite::run(CppUnit::TestResult*) (TestComposite.cpp:23)
==xxxxx==    by 0x5E42391: CppUnit::TestResult::runTest(CppUnit::Test*) (TestResult.cpp:145)
==xxxxx==    by 0x5E4464D: CppUnit::TestRunner::run(CppUnit::TestResult&, std::string const&) (TestRunner.cpp:96)
==xxxxx==    by 0x5E465BB: CppUnit::TextTestRunner::run(std::string, bool, bool, bool) (TextTestRunner.cpp:64)
==xxxxx==    by 0x40B659: coral::CoralCppUnitTest::Main() (CoralCppUnitTest.h:171)
==xxxxx==    by 0x40AAC9: main (test_MiscellaneousBugs.cpp:744)
==xxxxx== 
{
   <insert_a_suppression_name_here>
   Memcheck:Addr8
   fun:_ZN8frontier7Session5cleanEv
   fun:_ZN8frontier7AnyData5cleanEv
   fun:_ZN5coral14FrontierAccess9Statement5resetEv
   fun:_ZN5coral14FrontierAccess9StatementD1Ev
   fun:_ZN5coral14FrontierAccess6Cursor5closeEv
   fun:_ZN5coral14FrontierAccess6CursorD1Ev
   fun:_ZN5coral14FrontierAccess6CursorD0Ev
   fun:_ZN5coral14FrontierAccess5QueryD1Ev
   fun:_ZN5coral14FrontierAccess5QueryD0Ev
   fun:_ZNSt8auto_ptrIN5coral6IQueryEED1Ev
   fun:_ZN5coral26CoralMiscellaneousBugsTest18_test_reservedWordESs
   fun:_ZN5coral26CoralMiscellaneousBugsTest26test_bug91075_SYSTIMESTAMPEv
   fun:_ZN7CppUnit10TestCallerIN5coral26CoralMiscellaneousBugsTestEE7runTestEv
   fun:_ZNK7CppUnit21TestCaseMethodFunctorclEv
   fun:_ZN7CppUnit16DefaultProtector7protectERKNS_7FunctorERKNS_16ProtectorContextE
   fun:_ZN7CppUnit14ProtectorChain7protectERKNS_7FunctorERKNS_16ProtectorContextE
   fun:_ZN7CppUnit10TestResult7protectERKNS_7FunctorEPNS_4TestERKSs
   fun:_ZN7CppUnit8TestCase3runEPNS_10TestResultE
   fun:_ZN7CppUnit13TestComposite15doRunChildTestsEPNS_10TestResultE
   fun:_ZN7CppUnit13TestComposite3runEPNS_10TestResultE
   fun:_ZN7CppUnit13TestComposite15doRunChildTestsEPNS_10TestResultE
   fun:_ZN7CppUnit13TestComposite3runEPNS_10TestResultE
   fun:_ZN7CppUnit10TestResult7runTestEPNS_4TestE
   fun:_ZN7CppUnit10TestRunner3runERNS_10TestResultERKSs
}
--xxxxx-- 
--xxxxx-- used_suppression:      1 <oracle112030_OCIServerVersion_kpuvers_ttcpro_alloc_FAKE_LEAK> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:291 suppressed: 334,344 bytes in 1 blocks
--xxxxx-- used_suppression:     37 <oracle112030_OCIServerAttach_kpuatch_alloc_FAKE_LEAK> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:283 suppressed: 435,337 bytes in 5,669 blocks
--xxxxx-- used_suppression:    217 <oracle112030_OCIEnvCreate_kpummpin_alloc_FAKE_LEAK> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:178 suppressed: 289,361 bytes in 266 blocks
--xxxxx-- used_suppression:      1 <oracle112030_OCIDescribeAny_kpummealloc_TOBECHECKED_LEAK> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:838 suppressed: 18,152 bytes in 1 blocks
--xxxxx-- used_suppression:     44 <coral_PluginManager_loadLibrary_dlopen_alloc_FAKE_LEAK> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1766 suppressed: 37,650 bytes in 109 blocks
--xxxxx-- used_suppression:      1 <oracle112030_OCIEnvCreate_lxldalc_FAKE_LEAK> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:667 suppressed: 6,976 bytes in 1 blocks
--xxxxx-- used_suppression:      1 <oracle112030_OCIDescribeAny_kpummapg_TOBECHECKED_LEAK> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:821 suppressed: 4,216 bytes in 1 blocks
--xxxxx-- used_suppression:      4 <oracle112030_OCIEnvCreate_dlopen_alloc_FAKE_LEAK> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:187 suppressed: 1,520 bytes in 4 blocks
--xxxxx-- used_suppression:      1 <coral_PluginManager_loadLibrary_dlsym_alloc_FAKE_LEAK> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1798 suppressed: 121 bytes in 1 blocks
--xxxxx-- used_suppression:      1 <oracle112030_OCISessionBegin_kpuauth_calloc_FAKE_LEAK> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:573 suppressed: 16 bytes in 1 blocks
--xxxxx-- used_suppression:      1 <oracle112030_OCISessionBegin_A_X931RandomGenerateBytes_CMP_cond> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1358
--xxxxx-- used_suppression:      9 <oracle112030_OCISessionBegin_nassky_cond> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1411
--xxxxx-- used_suppression:     18 <oracle112030_OCISessionBegin_intelfastmemcpy_cond> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1464
--xxxxx-- used_suppression:      9 <oracle112030_OCISessionBegin_intelnewmemcpy_val8> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1316
--xxxxx-- used_suppression:     18 <oracle112030_OCISessionBegin_intelnewmemcpy_cond> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1446
--xxxxx-- used_suppression:    315 <oracle112030_OCISessionBegin_ztceb_unpadding_cond> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1427
--xxxxx-- used_suppression:    432 <oracle112030_OCISessionBegin_ztceadecbk_val8> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1233
--xxxxx-- used_suppression:   2016 <oracle112030_OCISessionBegin_ztceai_val8> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1258
--xxxxx-- used_suppression:    576 <oracle112030_OCISessionBegin_ztucbtx_val8> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1286
--xxxxx-- used_suppression:   2448 <oracle112030_OCISessionBegin_ztceaencbk_val8> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1217
--xxxxx-- used_suppression:      9 <oracle112030_OCISessionBegin_kzsrepw_cond> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1397
--xxxxx-- used_suppression:      9 <oracle112030_OCISessionBegin_ztvo5ke_val8> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1344
--xxxxx-- used_suppression:      9 <oracle112030_OCISessionBegin_ztvo5ke_cond> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1383
--xxxxx-- used_suppression:    545 <oracle112030_OCIServerVersion_snttwrite_libpthread_param> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1088
--xxxxx-- used_suppression:      2 <oracle112030_OCIServerAttach_intelnewmemcpy_bug98791_INVALIDREAD_addr16> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1029
--xxxxx-- used_suppression:      2 <oracle112030_OCIServerAttach_snttwrite_libpthread_param> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1111
--xxxxx-- used_suppression:    896 <oracle112030_OCIServerAttach_ztceaencbk_val8> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:506
--xxxxx-- used_suppression:   5594 <oracle112030_OCIServerAttach_A_X931RandomGenerateBytes_CMP_cond> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:521
--xxxxx-- used_suppression:    640 <oracle112030_OCIServerAttach_ztcedecb_val8> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:482
--xxxxx-- used_suppression:   4861 <oracle112030_OCIServerAttach_ztced_einit_val8> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:452
--xxxxx-- used_suppression:      5 dl-hack3-cond-1 /cvmfs/sft.cern.ch/lcg/releases/LCG_78root6/valgrind/3.10.0/x86_64-slc6-gcc48-dbg/lib/valgrind/default.supp:1206
==xxxxx== 
==xxxxx== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 18418 from 503)
