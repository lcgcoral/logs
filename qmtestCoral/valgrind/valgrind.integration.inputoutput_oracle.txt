==xxxxx== Memcheck, a memory error detector
==xxxxx== Copyright (C) 2002-2013, and GNU GPL'd, by Julian Seward et al.
==xxxxx== Using Valgrind-3.10.0 and LibVEX; rerun with -h for copyright info
==xxxxx== Command: test_Integration_InputOutput oracle
==xxxxx== Parent PID: xxxxx
==xxxxx== 
--xxxxx-- 
--xxxxx-- Valgrind options:
--xxxxx--    -v
--xxxxx--    --leak-check=full
--xxxxx--    --show-reachable=yes
--xxxxx--    --error-limit=no
--xxxxx--    --log-file=/home/avalassi/CORAL/trunk/logs/qmtestCoral/valgrind/x86_64-slc6-gcc48-dbg/valgrind.integration.inputoutput_oracle.txt
--xxxxx--    --suppressions=/home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp
--xxxxx--    --gen-suppressions=all
--xxxxx--    --num-callers=50
--xxxxx--    --track-origins=yes
--xxxxx-- Contents of /proc/version:
--xxxxx--   Linux version 2.6.32-504.16.2.el6.x86_64 (mockbuild@lxsoft14.cern.ch) (gcc version 4.4.7 20120313 (Red Hat 4.4.7-11) (GCC) ) #1 SMP Tue Apr 21 21:44:51 CEST 2015
--xxxxx-- Arch and hwcaps: AMD64, LittleEndian, amd64-cx16-sse3
--xxxxx-- Page sizes: currently 4096, max supported 4096
--xxxxx-- Valgrind library directory: /cvmfs/sft.cern.ch/lcg/releases/LCG_78root6/valgrind/3.10.0/x86_64-slc6-gcc48-dbg/lib/valgrind
--xxxxx-- Reading syms from /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/tests/bin/test_Integration_InputOutput
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/valgrind/3.10.0-073ad/x86_64-slc6-gcc48-dbg/lib/valgrind/memcheck-amd64-linux
--xxxxx--    object doesn't have a dynamic symbol table
--xxxxx-- Reading syms from /lib64/ld-2.12.so
--xxxxx-- Scheduler: using generic scheduler lock implementation.
--xxxxx-- Reading suppressions file: /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp
--xxxxx-- Reading suppressions file: /cvmfs/sft.cern.ch/lcg/releases/LCG_78root6/valgrind/3.10.0/x86_64-slc6-gcc48-dbg/lib/valgrind/default.supp
==xxxxx== embedded gdbserver: reading from /tmp/vgdb-pipe-from-vgdb-to-xxxxx-by-avalassi-on-aicoral61.cern.ch
==xxxxx== embedded gdbserver: writing to   /tmp/vgdb-pipe-to-vgdb-from-xxxxx-by-avalassi-on-aicoral61.cern.ch
==xxxxx== embedded gdbserver: shared mem   /tmp/vgdb-pipe-shared-mem-vgdb-xxxxx-by-avalassi-on-aicoral61.cern.ch
==xxxxx== 
==xxxxx== TO CONTROL THIS PROCESS USING vgdb (which you probably
==xxxxx== don't want to do, unless you know exactly what you're doing,
==xxxxx== or are doing some strange experiment):
==xxxxx==   /cvmfs/sft.cern.ch/lcg/releases/LCG_78root6/valgrind/3.10.0/x86_64-slc6-gcc48-dbg/lib/valgrind/../../bin/vgdb --pid=xxxxx ...command...
==xxxxx== 
==xxxxx== TO DEBUG THIS PROCESS USING GDB: start GDB like this
==xxxxx==   /path/to/gdb test_Integration_InputOutput
==xxxxx== and then give GDB the following command
==xxxxx==   target remote | /cvmfs/sft.cern.ch/lcg/releases/LCG_78root6/valgrind/3.10.0/x86_64-slc6-gcc48-dbg/lib/valgrind/../../bin/vgdb --pid=xxxxx
==xxxxx== --pid is optional if only one valgrind process is running
==xxxxx== 
--xxxxx-- REDIR: 0x3b5bc17c30 (ld-linux-x86-64.so.2:strlen) redirected to 0x3806bd71 (vgPlain_amd64_linux_REDIR_FOR_strlen)
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/valgrind/3.10.0-073ad/x86_64-slc6-gcc48-dbg/lib/valgrind/vgpreload_core-amd64-linux.so
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/valgrind/3.10.0-073ad/x86_64-slc6-gcc48-dbg/lib/valgrind/vgpreload_memcheck-amd64-linux.so
==xxxxx== WARNING: new redirection conflicts with existing -- ignoring it
--xxxxx--     old: 0x3b5bc17c30 (strlen              ) R-> (0000.0) 0x3806bd71 vgPlain_amd64_linux_REDIR_FOR_strlen
--xxxxx--     new: 0x3b5bc17c30 (strlen              ) R-> (2007.0) 0x04a09b30 strlen
--xxxxx-- REDIR: 0x3b5bc17a40 (ld-linux-x86-64.so.2:index) redirected to 0x4a096e0 (index)
--xxxxx-- REDIR: 0x3b5bc17ac0 (ld-linux-x86-64.so.2:strcmp) redirected to 0x4a0ac80 (strcmp)
--xxxxx-- REDIR: 0x3b5bc18a10 (ld-linux-x86-64.so.2:mempcpy) redirected to 0x4a0d730 (mempcpy)
--xxxxx-- Reading syms from /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/tests/lib/libtest_TestEnv.so
--xxxxx-- Reading syms from /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/lib/liblcg_CoralCommon.so
--xxxxx-- Reading syms from /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/lib/liblcg_RelationalAccess.so
--xxxxx-- Reading syms from /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/lib/liblcg_CoralKernel.so
--xxxxx-- Reading syms from /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/lib/liblcg_CoralBase.so
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/Boost/1.55.0_python2.7-dcbb8/x86_64-slc6-gcc48-dbg/lib/libboost_thread-gcc48-mt-d-1_55.so.1.55.0
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/Boost/1.55.0_python2.7-dcbb8/x86_64-slc6-gcc48-dbg/lib/libboost_system-gcc48-mt-d-1_55.so.1.55.0
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/Boost/1.55.0_python2.7-dcbb8/x86_64-slc6-gcc48-dbg/lib/libboost_filesystem-gcc48-mt-d-1_55.so.1.55.0
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/CppUnit/1.12.1_p1-31a6a/x86_64-slc6-gcc48-dbg/lib/libcppunit-1.12.so.1.0.0
--xxxxx-- Reading syms from /lib64/libdl-2.12.so
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/Boost/1.55.0_python2.7-dcbb8/x86_64-slc6-gcc48-dbg/lib/libboost_date_time-gcc48-mt-d-1_55.so.1.55.0
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/gcc/4.8.4/x86_64-slc6/lib64/libstdc++.so.6.0.19
--xxxxx-- Reading syms from /lib64/libm-2.12.so
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/gcc/4.8.4/x86_64-slc6/lib64/libgcc_s.so.1
--xxxxx-- Reading syms from /lib64/libpthread-2.12.so
--xxxxx-- Reading syms from /lib64/libc-2.12.so
--xxxxx-- Reading syms from /lib64/librt-2.12.so
--xxxxx-- REDIR: 0x3b5c084d70 (libc.so.6:strcasecmp) redirected to 0x4801696 (_vgnU_ifunc_wrapper)
--xxxxx-- REDIR: 0x3b5c087030 (libc.so.6:strncasecmp) redirected to 0x4801696 (_vgnU_ifunc_wrapper)
--xxxxx-- REDIR: 0x3b5c081200 (libc.so.6:__GI_strlen) redirected to 0x4a09a90 (__GI_strlen)
--xxxxx-- REDIR: 0x3b5c082ce0 (libc.so.6:__GI_strrchr) redirected to 0x4a093f0 (__GI_strrchr)
--xxxxx-- REDIR: 0x3b5c07f780 (libc.so.6:strcmp) redirected to 0x4801696 (_vgnU_ifunc_wrapper)
--xxxxx-- REDIR: 0x3b5c128450 (libc.so.6:__strcmp_sse42) redirected to 0x4a0ac30 (__strcmp_sse42)
--xxxxx-- REDIR: 0x62ea2b0 (libstdc++.so.6:operator new(unsigned long)) redirected to 0x4a06f4e (operator new(unsigned long))
--xxxxx-- REDIR: 0x3b5c089710 (libc.so.6:memcpy) redirected to 0x4a0b330 (memcpy)
--xxxxx-- REDIR: 0x3b5c0811c0 (libc.so.6:strlen) redirected to 0x4801696 (_vgnU_ifunc_wrapper)
--xxxxx-- REDIR: 0x3b5c133720 (libc.so.6:__strlen_sse42) redirected to 0x4a09af0 (__strlen_sse42)
--xxxxx-- REDIR: 0x3b5c07a320 (libc.so.6:calloc) redirected to 0x4a087ae (calloc)
--xxxxx-- REDIR: 0x3b5c081430 (libc.so.6:__GI_strncmp) redirected to 0x4a0a2c0 (__GI_strncmp)
--xxxxx-- REDIR: 0x3b5c083470 (libc.so.6:bcmp) redirected to 0x4801696 (_vgnU_ifunc_wrapper)
--xxxxx-- REDIR: 0x3b5c13e7d0 (libc.so.6:__memcmp_sse4_1) redirected to 0x4a0c590 (__memcmp_sse4_1)
--xxxxx-- REDIR: 0x62e8570 (libstdc++.so.6:operator delete(void*)) redirected to 0x4a07f7a (operator delete(void*))
--xxxxx-- REDIR: 0x3b5c0845a0 (libc.so.6:mempcpy) redirected to 0x4a0d460 (mempcpy)
--xxxxx-- REDIR: 0x3b5c035770 (libc.so.6:setenv) redirected to 0x4a0dff0 (setenv)
--xxxxx-- REDIR: 0x3b5c07f700 (libc.so.6:__GI_strchr) redirected to 0x4a09520 (__GI_strchr)
--xxxxx-- REDIR: 0x3b5c07bab0 (libc.so.6:realloc) redirected to 0x4a08950 (realloc)
--xxxxx-- REDIR: 0x3b5c07a6e0 (libc.so.6:malloc) redirected to 0x4a06a46 (malloc)
--xxxxx-- Reading syms from /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/lib/liblcg_ConnectionService.so
--xxxxx-- REDIR: 0x3b5c07b5c0 (libc.so.6:free) redirected to 0x4a07b60 (free)
--xxxxx-- REDIR: 0x3b5c08ea20 (libc.so.6:__GI_strstr) redirected to 0x4a0d9c0 (__strstr_sse2)
--xxxxx-- REDIR: 0x3b5c080c40 (libc.so.6:__GI_strcpy) redirected to 0x4a09c30 (__GI_strcpy)
--xxxxx-- REDIR: 0x3b5c07f7c0 (libc.so.6:__GI_strcmp) redirected to 0x4a0ab90 (__GI_strcmp)
--xxxxx-- Reading syms from /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/lib/liblcg_RelationalService.so
--xxxxx-- REDIR: 0x3b5bc18b60 (ld-linux-x86-64.so.2:stpcpy) redirected to 0x4a0cab0 (stpcpy)
--xxxxx-- Reading syms from /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/lib/liblcg_XMLLookupService.so
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/XercesC/3.1.1p1-8ccd5/x86_64-slc6-gcc48-dbg/lib/libxerces-c-3.1.so
--xxxxx-- Reading syms from /lib64/libnsl-2.12.so
--xxxxx-- REDIR: 0x3b5c0838a0 (libc.so.6:memmove) redirected to 0x4a0cd90 (memmove)
--xxxxx-- REDIR: 0x3b5c07f6d0 (libc.so.6:index) redirected to 0x4801696 (_vgnU_ifunc_wrapper)
--xxxxx-- REDIR: 0x3b5c1283a0 (libc.so.6:__strchr_sse42) redirected to 0x4a095e0 (index)
--xxxxx-- Reading syms from /usr/lib64/gconv/UTF-16.so
--xxxxx-- REDIR: 0x3b5c12b4f0 (libc.so.6:__strcasecmp_sse42) redirected to 0x4a0a410 (strcasecmp)
--xxxxx-- REDIR: 0x3b5c083a40 (libc.so.6:memset) redirected to 0x4801696 (_vgnU_ifunc_wrapper)
--xxxxx-- REDIR: 0x3b5c083a80 (libc.so.6:__GI_memset) redirected to 0x4a0cce0 (memset)
--xxxxx-- REDIR: 0x3b5c0813f0 (libc.so.6:strncmp) redirected to 0x4801696 (_vgnU_ifunc_wrapper)
--xxxxx-- REDIR: 0x3b5c1292f0 (libc.so.6:__strncmp_sse42) redirected to 0x4a0a3a0 (__strncmp_sse42)
--xxxxx-- Reading syms from /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/lib/liblcg_OracleAccess.so
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libnnz11.so
--xxxxx-- Reading syms from /lib64/libaio.so.1.0.1
--xxxxx--    object doesn't have a symbol table
--xxxxx-- Reading syms from /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/lib/liblcg_XMLAuthenticationService.so
--xxxxx-- REDIR: 0x3b5c082c80 (libc.so.6:strncpy) redirected to 0x4801696 (_vgnU_ifunc_wrapper)
--xxxxx-- REDIR: 0x3b5c134be0 (libc.so.6:???) redirected to 0x4a09d10 (strncpy)
--xxxxx-- REDIR: 0x3b5c08ab90 (libc.so.6:strchrnul) redirected to 0x4a0d340 (strchrnul)
--xxxxx-- REDIR: 0x3b5c081350 (libc.so.6:strncat) redirected to 0x4a09900 (strncat)
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libociicus.so
--xxxxx-- REDIR: 0x3b5c0350e0 (libc.so.6:putenv) redirected to 0x4a0deb0 (putenv)
--xxxxx-- REDIR: 0x3b5c0812e0 (libc.so.6:strnlen) redirected to 0x4a09a10 (strnlen)
--xxxxx-- REDIR: 0x3b5c08e560 (libc.so.6:strstr) redirected to 0x4801696 (_vgnU_ifunc_wrapper)
--xxxxx-- REDIR: 0x3b5c12a650 (libc.so.6:__strstr_sse42) redirected to 0x4a0da50 (__strstr_sse42)
--xxxxx-- Reading syms from /usr/lib64/libnuma.so.1
--xxxxx--    object doesn't have a symbol table
--xxxxx-- REDIR: 0x3b5c0833f0 (libc.so.6:memchr) redirected to 0x4a0ad20 (memchr)
--xxxxx-- REDIR: 0x3b5c08eeb0 (libc.so.6:strcasestr) redirected to 0x4801696 (_vgnU_ifunc_wrapper)
--xxxxx-- REDIR: 0x3b5c12f240 (libc.so.6:__strcasestr_sse42) redirected to 0x4a0dc20 (strcasestr)
--xxxxx-- REDIR: 0x3b5c082cb0 (libc.so.6:rindex) redirected to 0x4801696 (_vgnU_ifunc_wrapper)
--xxxxx-- REDIR: 0x3b5c12a4b0 (libc.so.6:__strrchr_sse42) redirected to 0x4a09480 (__strrchr_sse42)
--xxxxx-- REDIR: 0x3b5c08ab40 (libc.so.6:__GI___rawmemchr) redirected to 0x4a0d3a0 (__GI___rawmemchr)
--xxxxx-- REDIR: 0x3b5c12d190 (libc.so.6:__strncasecmp_sse42) redirected to 0x4a0a4f0 (strncasecmp)
--xxxxx-- REDIR: 0xffffffffff600000 (???:???) redirected to 0x3806bd53 (vgPlain_amd64_linux_REDIR_FOR_vgettimeofday)
--xxxxx-- REDIR: 0x3b5c084c20 (libc.so.6:__GI_stpcpy) redirected to 0x4a0c730 (__GI_stpcpy)
--xxxxx-- Reading syms from /lib64/libnss_files-2.12.so
--xxxxx-- Reading syms from /lib64/libnss_sss.so.2
--xxxxx--    object doesn't have a symbol table
--xxxxx-- REDIR: 0xffffffffff600400 (???:???) redirected to 0x3806bd5d (vgPlain_amd64_linux_REDIR_FOR_vtime)
--xxxxx-- REDIR: 0x3b5c08de80 (libc.so.6:__GI_strncpy) redirected to 0x4a09e60 (__GI_strncpy)
--xxxxx-- REDIR: 0xb10ccb0 (NONE:_intel_fast_memcpy) redirected to 0x4a0c1e0 (_intel_fast_memcpy)
--xxxxx-- REDIR: 0x62ea3c0 (libstdc++.so.6:operator new[](unsigned long)) redirected to 0x4a075f3 (operator new[](unsigned long))
--xxxxx-- REDIR: 0x62e85a0 (libstdc++.so.6:operator delete[](void*)) redirected to 0x4a0842a (operator delete[](void*))
==xxxxx== Invalid read of size 16
==xxxxx==    at 0x980DA49: __intel_new_memcpy (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0x904F39C: ttciovconv (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0x8CF3D1B: kpulbcs (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0xA85EB91: ttcdrv (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0xA80B480: nioqwa (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0xA7F9190: upirtrc (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0xA7FF7E1: kpurcsc (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0x8CDDE7B: kpulfwrarr (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0x8CDD221: kpulfwr (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0x8C8A608: OCILobWriteAppend (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0x859C9F0: coral::OracleAccess::PolymorphicTVector<coral::Blob>::push_back(void const*, short) (PolymorphicVector.h:430)
==xxxxx==    by 0x859A431: coral::OracleAccess::BulkOperation::processNextIteration() (BulkOperation.cpp:162)
==xxxxx==    by 0x40825D: coral::InputOutput::writeBulk() (test_Integration_InputOutput.cpp:153)
==xxxxx==    by 0x40E9CD: main (test_Integration_InputOutput.cpp:532)
==xxxxx==  Address 0xba814a0 is 26,992 bytes inside a block of size 27,000 alloc'd
==xxxxx==    at 0x4A08A1A: realloc (vg_replace_malloc.c:692)
==xxxxx==    by 0x55A63C6: coral::Blob::resize(long) (Blob.cpp:81)
==xxxxx==    by 0x4C2C977: Testing::fillData(int) (Testing.cpp:265)
==xxxxx==    by 0x408246: coral::InputOutput::writeBulk() (test_Integration_InputOutput.cpp:152)
==xxxxx==    by 0x40E9CD: main (test_Integration_InputOutput.cpp:532)
==xxxxx== 
{
   <insert_a_suppression_name_here>
   Memcheck:Addr16
   fun:__intel_new_memcpy
   fun:ttciovconv
   fun:kpulbcs
   fun:ttcdrv
   fun:nioqwa
   fun:upirtrc
   fun:kpurcsc
   fun:kpulfwrarr
   fun:kpulfwr
   fun:OCILobWriteAppend
   fun:_ZN5coral12OracleAccess18PolymorphicTVectorINS_4BlobEE9push_backEPKvs
   fun:_ZN5coral12OracleAccess13BulkOperation20processNextIterationEv
   fun:_ZN5coral11InputOutput9writeBulkEv
   fun:main
}
==xxxxx== Invalid read of size 16
==xxxxx==    at 0x980D955: __intel_new_memcpy (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0x904F39C: ttciovconv (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0x8CF3D1B: kpulbcs (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0xA85EB91: ttcdrv (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0xA80B480: nioqwa (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0xA7F9190: upirtrc (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0xA7FF7E1: kpurcsc (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0x8CDDE7B: kpulfwrarr (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0x8CDD221: kpulfwr (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0x8C8A608: OCILobWriteAppend (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0x859C9F0: coral::OracleAccess::PolymorphicTVector<coral::Blob>::push_back(void const*, short) (PolymorphicVector.h:430)
==xxxxx==    by 0x859A431: coral::OracleAccess::BulkOperation::processNextIteration() (BulkOperation.cpp:162)
==xxxxx==    by 0x40825D: coral::InputOutput::writeBulk() (test_Integration_InputOutput.cpp:153)
==xxxxx==    by 0x40E9CD: main (test_Integration_InputOutput.cpp:532)
==xxxxx==  Address 0xd268ff0 is 52,992 bytes inside a block of size 53,000 alloc'd
==xxxxx==    at 0x4A08A1A: realloc (vg_replace_malloc.c:692)
==xxxxx==    by 0x55A63C6: coral::Blob::resize(long) (Blob.cpp:81)
==xxxxx==    by 0x4C2C977: Testing::fillData(int) (Testing.cpp:265)
==xxxxx==    by 0x408246: coral::InputOutput::writeBulk() (test_Integration_InputOutput.cpp:152)
==xxxxx==    by 0x40E9CD: main (test_Integration_InputOutput.cpp:532)
==xxxxx== 
{
   <insert_a_suppression_name_here>
   Memcheck:Addr16
   fun:__intel_new_memcpy
   fun:ttciovconv
   fun:kpulbcs
   fun:ttcdrv
   fun:nioqwa
   fun:upirtrc
   fun:kpurcsc
   fun:kpulfwrarr
   fun:kpulfwr
   fun:OCILobWriteAppend
   fun:_ZN5coral12OracleAccess18PolymorphicTVectorINS_4BlobEE9push_backEPKvs
   fun:_ZN5coral12OracleAccess13BulkOperation20processNextIterationEv
   fun:_ZN5coral11InputOutput9writeBulkEv
   fun:main
}
--xxxxx-- Discarding syms at 0x824d580-0x824ecd8 in /usr/lib64/gconv/UTF-16.so due to munmap()
--xxxxx-- Discarding syms at 0xc5161f0-0xc51e648 in /lib64/libnss_files-2.12.so due to munmap()
--xxxxx-- Discarding syms at 0xc723730-0xc728278 in /lib64/libnss_sss.so.2 due to munmap()
==xxxxx== 
==xxxxx== HEAP SUMMARY:
==xxxxx==     in use at exit: 1,106,360 bytes in 5,992 blocks
==xxxxx==   total heap usage: 31,512 allocs, 25,520 frees, 56,923,664 bytes allocated
==xxxxx== 
==xxxxx== Searching for pointers to 5,992 not-freed blocks
==xxxxx== Checked 3,917,728 bytes
==xxxxx== 
==xxxxx== LEAK SUMMARY:
==xxxxx==    definitely lost: 0 bytes in 0 blocks
==xxxxx==    indirectly lost: 0 bytes in 0 blocks
==xxxxx==      possibly lost: 0 bytes in 0 blocks
==xxxxx==    still reachable: 0 bytes in 0 blocks
==xxxxx==         suppressed: 1,106,360 bytes in 5,992 blocks
==xxxxx== 
==xxxxx== ERROR SUMMARY: 2 errors from 2 contexts (suppressed: 13070 from 504)
==xxxxx== 
==xxxxx== 1 errors in context 1 of 2:
==xxxxx== Invalid read of size 16
==xxxxx==    at 0x980D955: __intel_new_memcpy (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0x904F39C: ttciovconv (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0x8CF3D1B: kpulbcs (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0xA85EB91: ttcdrv (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0xA80B480: nioqwa (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0xA7F9190: upirtrc (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0xA7FF7E1: kpurcsc (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0x8CDDE7B: kpulfwrarr (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0x8CDD221: kpulfwr (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0x8C8A608: OCILobWriteAppend (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0x859C9F0: coral::OracleAccess::PolymorphicTVector<coral::Blob>::push_back(void const*, short) (PolymorphicVector.h:430)
==xxxxx==    by 0x859A431: coral::OracleAccess::BulkOperation::processNextIteration() (BulkOperation.cpp:162)
==xxxxx==    by 0x40825D: coral::InputOutput::writeBulk() (test_Integration_InputOutput.cpp:153)
==xxxxx==    by 0x40E9CD: main (test_Integration_InputOutput.cpp:532)
==xxxxx==  Address 0xd268ff0 is 52,992 bytes inside a block of size 53,000 alloc'd
==xxxxx==    at 0x4A08A1A: realloc (vg_replace_malloc.c:692)
==xxxxx==    by 0x55A63C6: coral::Blob::resize(long) (Blob.cpp:81)
==xxxxx==    by 0x4C2C977: Testing::fillData(int) (Testing.cpp:265)
==xxxxx==    by 0x408246: coral::InputOutput::writeBulk() (test_Integration_InputOutput.cpp:152)
==xxxxx==    by 0x40E9CD: main (test_Integration_InputOutput.cpp:532)
==xxxxx== 
{
   <insert_a_suppression_name_here>
   Memcheck:Addr16
   fun:__intel_new_memcpy
   fun:ttciovconv
   fun:kpulbcs
   fun:ttcdrv
   fun:nioqwa
   fun:upirtrc
   fun:kpurcsc
   fun:kpulfwrarr
   fun:kpulfwr
   fun:OCILobWriteAppend
   fun:_ZN5coral12OracleAccess18PolymorphicTVectorINS_4BlobEE9push_backEPKvs
   fun:_ZN5coral12OracleAccess13BulkOperation20processNextIterationEv
   fun:_ZN5coral11InputOutput9writeBulkEv
   fun:main
}
==xxxxx== 
==xxxxx== 1 errors in context 2 of 2:
==xxxxx== Invalid read of size 16
==xxxxx==    at 0x980DA49: __intel_new_memcpy (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0x904F39C: ttciovconv (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0x8CF3D1B: kpulbcs (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0xA85EB91: ttcdrv (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0xA80B480: nioqwa (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0xA7F9190: upirtrc (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0xA7FF7E1: kpurcsc (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0x8CDDE7B: kpulfwrarr (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0x8CDD221: kpulfwr (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0x8C8A608: OCILobWriteAppend (in /cvmfs/sft.cern.ch/lcg/releases/oracle/11.2.0.3.0-e33b7/x86_64-slc6-gcc48-dbg/lib/libclntsh.so.11.1)
==xxxxx==    by 0x859C9F0: coral::OracleAccess::PolymorphicTVector<coral::Blob>::push_back(void const*, short) (PolymorphicVector.h:430)
==xxxxx==    by 0x859A431: coral::OracleAccess::BulkOperation::processNextIteration() (BulkOperation.cpp:162)
==xxxxx==    by 0x40825D: coral::InputOutput::writeBulk() (test_Integration_InputOutput.cpp:153)
==xxxxx==    by 0x40E9CD: main (test_Integration_InputOutput.cpp:532)
==xxxxx==  Address 0xba814a0 is 26,992 bytes inside a block of size 27,000 alloc'd
==xxxxx==    at 0x4A08A1A: realloc (vg_replace_malloc.c:692)
==xxxxx==    by 0x55A63C6: coral::Blob::resize(long) (Blob.cpp:81)
==xxxxx==    by 0x4C2C977: Testing::fillData(int) (Testing.cpp:265)
==xxxxx==    by 0x408246: coral::InputOutput::writeBulk() (test_Integration_InputOutput.cpp:152)
==xxxxx==    by 0x40E9CD: main (test_Integration_InputOutput.cpp:532)
==xxxxx== 
{
   <insert_a_suppression_name_here>
   Memcheck:Addr16
   fun:__intel_new_memcpy
   fun:ttciovconv
   fun:kpulbcs
   fun:ttcdrv
   fun:nioqwa
   fun:upirtrc
   fun:kpurcsc
   fun:kpulfwrarr
   fun:kpulfwr
   fun:OCILobWriteAppend
   fun:_ZN5coral12OracleAccess18PolymorphicTVectorINS_4BlobEE9push_backEPKvs
   fun:_ZN5coral12OracleAccess13BulkOperation20processNextIterationEv
   fun:_ZN5coral11InputOutput9writeBulkEv
   fun:main
}
--xxxxx-- 
--xxxxx-- used_suppression:      1 <oracle112030_OCIServerVersion_kpuvers_ttcpro_alloc_FAKE_LEAK> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:291 suppressed: 334,344 bytes in 1 blocks
--xxxxx-- used_suppression:     37 <oracle112030_OCIServerAttach_kpuatch_alloc_FAKE_LEAK> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:283 suppressed: 435,331 bytes in 5,669 blocks
--xxxxx-- used_suppression:    217 <oracle112030_OCIEnvCreate_kpummpin_alloc_FAKE_LEAK> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:178 suppressed: 289,361 bytes in 266 blocks
--xxxxx-- used_suppression:      1 <oracle112030_OCIDescribeAny_kpummealloc_TOBECHECKED_LEAK> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:838 suppressed: 18,152 bytes in 1 blocks
--xxxxx-- used_suppression:      1 <oracle112030_OCIEnvCreate_lxldalc_FAKE_LEAK> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:667 suppressed: 6,976 bytes in 1 blocks
--xxxxx-- used_suppression:      1 <oracle112030_OCIDescribeAny_kpummapg_TOBECHECKED_LEAK> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:821 suppressed: 4,216 bytes in 1 blocks
--xxxxx-- used_suppression:     35 <coral_PluginManager_loadLibrary_dlopen_alloc_FAKE_LEAK> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1766 suppressed: 16,444 bytes in 48 blocks
--xxxxx-- used_suppression:      4 <oracle112030_OCIEnvCreate_dlopen_alloc_FAKE_LEAK> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:187 suppressed: 1,520 bytes in 4 blocks
--xxxxx-- used_suppression:      1 <oracle112030_OCISessionBegin_kpuauth_calloc_FAKE_LEAK> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:573 suppressed: 16 bytes in 1 blocks
--xxxxx-- used_suppression:      1 <oracle112030_OCIStmtPrepare2_intelnewmemcpy_bug98791_INVALIDREAD_addr8> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:881
--xxxxx-- used_suppression:      6 <oracle112030_OCISessionBegin_nassky_cond> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1411
--xxxxx-- used_suppression:     12 <oracle112030_OCISessionBegin_intelfastmemcpy_cond> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1464
--xxxxx-- used_suppression:      6 <oracle112030_OCISessionBegin_intelnewmemcpy_val8> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1316
--xxxxx-- used_suppression:     12 <oracle112030_OCISessionBegin_intelnewmemcpy_cond> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1446
--xxxxx-- used_suppression:    210 <oracle112030_OCISessionBegin_ztceb_unpadding_cond> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1427
--xxxxx-- used_suppression:    288 <oracle112030_OCISessionBegin_ztceadecbk_val8> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1233
--xxxxx-- used_suppression:   1344 <oracle112030_OCISessionBegin_ztceai_val8> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1258
--xxxxx-- used_suppression:    384 <oracle112030_OCISessionBegin_ztucbtx_val8> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1286
--xxxxx-- used_suppression:   1632 <oracle112030_OCISessionBegin_ztceaencbk_val8> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1217
--xxxxx-- used_suppression:      1 <oracle112030_OCISessionBegin_A_X931RandomGenerateBytes_CMP_cond> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1358
--xxxxx-- used_suppression:      6 <oracle112030_OCISessionBegin_kzsrepw_cond> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1397
--xxxxx-- used_suppression:      6 <oracle112030_OCISessionBegin_ztvo5ke_val8> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1344
--xxxxx-- used_suppression:      6 <oracle112030_OCISessionBegin_ztvo5ke_cond> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1383
--xxxxx-- used_suppression:      6 <oracle112030_OCIAttrSet_A_X931RandomGenerateBytes_CMP_cond> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1368
--xxxxx-- used_suppression:    807 <oracle112030_OCIServerVersion_snttwrite_libpthread_param> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1088
--xxxxx-- used_suppression:      2 <oracle112030_OCIServerAttach_intelnewmemcpy_bug98791_INVALIDREAD_addr16> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1029
--xxxxx-- used_suppression:      2 <oracle112030_OCIServerAttach_snttwrite_libpthread_param> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:1111
--xxxxx-- used_suppression:    608 <oracle112030_OCIServerAttach_ztceaencbk_val8> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:506
--xxxxx-- used_suppression:   3757 <oracle112030_OCIServerAttach_A_X931RandomGenerateBytes_CMP_cond> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:521
--xxxxx-- used_suppression:    640 <oracle112030_OCIServerAttach_ztcedecb_val8> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:482
--xxxxx-- used_suppression:   3325 <oracle112030_OCIServerAttach_ztced_einit_val8> /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp:452
--xxxxx-- used_suppression:      5 dl-hack3-cond-1 /cvmfs/sft.cern.ch/lcg/releases/LCG_78root6/valgrind/3.10.0/x86_64-slc6-gcc48-dbg/lib/valgrind/default.supp:1206
==xxxxx== 
==xxxxx== ERROR SUMMARY: 2 errors from 2 contexts (suppressed: 13070 from 504)
