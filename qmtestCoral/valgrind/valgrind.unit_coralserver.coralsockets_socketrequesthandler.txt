==xxxxx== Memcheck, a memory error detector
==xxxxx== Copyright (C) 2002-2013, and GNU GPL'd, by Julian Seward et al.
==xxxxx== Using Valgrind-3.10.0 and LibVEX; rerun with -h for copyright info
==xxxxx== Command: test_CoralSockets_SocketRequestHandler
==xxxxx== Parent PID: xxxxx
==xxxxx== 
--xxxxx-- 
--xxxxx-- Valgrind options:
--xxxxx--    -v
--xxxxx--    --leak-check=full
--xxxxx--    --show-reachable=yes
--xxxxx--    --error-limit=no
--xxxxx--    --log-file=/home/avalassi/CORAL/trunk/logs/qmtestCoral/valgrind/x86_64-slc6-gcc48-dbg/valgrind.unit_coralserver.coralsockets_socketrequesthandler.txt
--xxxxx--    --suppressions=/home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp
--xxxxx--    --gen-suppressions=all
--xxxxx--    --num-callers=50
--xxxxx--    --track-origins=yes
--xxxxx-- Contents of /proc/version:
--xxxxx--   Linux version 2.6.32-504.16.2.el6.x86_64 (mockbuild@lxsoft14.cern.ch) (gcc version 4.4.7 20120313 (Red Hat 4.4.7-11) (GCC) ) #1 SMP Tue Apr 21 21:44:51 CEST 2015
--xxxxx-- Arch and hwcaps: AMD64, LittleEndian, amd64-cx16-sse3
--xxxxx-- Page sizes: currently 4096, max supported 4096
--xxxxx-- Valgrind library directory: /cvmfs/sft.cern.ch/lcg/releases/LCG_78root6/valgrind/3.10.0/x86_64-slc6-gcc48-dbg/lib/valgrind
--xxxxx-- Reading syms from /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/tests/bin/test_CoralSockets_SocketRequestHandler
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/valgrind/3.10.0-073ad/x86_64-slc6-gcc48-dbg/lib/valgrind/memcheck-amd64-linux
--xxxxx--    object doesn't have a dynamic symbol table
--xxxxx-- Reading syms from /lib64/ld-2.12.so
--xxxxx-- Scheduler: using generic scheduler lock implementation.
--xxxxx-- Reading suppressions file: /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/bin/valgrind.supp
--xxxxx-- Reading suppressions file: /cvmfs/sft.cern.ch/lcg/releases/LCG_78root6/valgrind/3.10.0/x86_64-slc6-gcc48-dbg/lib/valgrind/default.supp
==xxxxx== embedded gdbserver: reading from /tmp/vgdb-pipe-from-vgdb-to-xxxxx-by-avalassi-on-aicoral61.cern.ch
==xxxxx== embedded gdbserver: writing to   /tmp/vgdb-pipe-to-vgdb-from-xxxxx-by-avalassi-on-aicoral61.cern.ch
==xxxxx== embedded gdbserver: shared mem   /tmp/vgdb-pipe-shared-mem-vgdb-xxxxx-by-avalassi-on-aicoral61.cern.ch
==xxxxx== 
==xxxxx== TO CONTROL THIS PROCESS USING vgdb (which you probably
==xxxxx== don't want to do, unless you know exactly what you're doing,
==xxxxx== or are doing some strange experiment):
==xxxxx==   /cvmfs/sft.cern.ch/lcg/releases/LCG_78root6/valgrind/3.10.0/x86_64-slc6-gcc48-dbg/lib/valgrind/../../bin/vgdb --pid=xxxxx ...command...
==xxxxx== 
==xxxxx== TO DEBUG THIS PROCESS USING GDB: start GDB like this
==xxxxx==   /path/to/gdb test_CoralSockets_SocketRequestHandler
==xxxxx== and then give GDB the following command
==xxxxx==   target remote | /cvmfs/sft.cern.ch/lcg/releases/LCG_78root6/valgrind/3.10.0/x86_64-slc6-gcc48-dbg/lib/valgrind/../../bin/vgdb --pid=xxxxx
==xxxxx== --pid is optional if only one valgrind process is running
==xxxxx== 
--xxxxx-- REDIR: 0x3b5bc17c30 (ld-linux-x86-64.so.2:strlen) redirected to 0x3806bd71 (vgPlain_amd64_linux_REDIR_FOR_strlen)
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/valgrind/3.10.0-073ad/x86_64-slc6-gcc48-dbg/lib/valgrind/vgpreload_core-amd64-linux.so
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/valgrind/3.10.0-073ad/x86_64-slc6-gcc48-dbg/lib/valgrind/vgpreload_memcheck-amd64-linux.so
==xxxxx== WARNING: new redirection conflicts with existing -- ignoring it
--xxxxx--     old: 0x3b5bc17c30 (strlen              ) R-> (0000.0) 0x3806bd71 vgPlain_amd64_linux_REDIR_FOR_strlen
--xxxxx--     new: 0x3b5bc17c30 (strlen              ) R-> (2007.0) 0x04a09b30 strlen
--xxxxx-- REDIR: 0x3b5bc17a40 (ld-linux-x86-64.so.2:index) redirected to 0x4a096e0 (index)
--xxxxx-- REDIR: 0x3b5bc17ac0 (ld-linux-x86-64.so.2:strcmp) redirected to 0x4a0ac80 (strcmp)
--xxxxx-- REDIR: 0x3b5bc18a10 (ld-linux-x86-64.so.2:mempcpy) redirected to 0x4a0d730 (mempcpy)
--xxxxx-- Reading syms from /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/lib/liblcg_CoralSockets.so
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/CppUnit/1.12.1_p1-31a6a/x86_64-slc6-gcc48-dbg/lib/libcppunit-1.12.so.1.0.0
--xxxxx-- Reading syms from /lib64/libdl-2.12.so
--xxxxx-- Reading syms from /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/lib/liblcg_CoralServerBase.so
--xxxxx-- Reading syms from /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/lib/liblcg_RelationalAccess.so
--xxxxx-- Reading syms from /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/lib/liblcg_CoralKernel.so
--xxxxx-- Reading syms from /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/lib/liblcg_CoralMonitor.so
--xxxxx-- Reading syms from /home/avalassi/CORAL/trunk/x86_64-slc6-gcc48-dbg/lib/liblcg_CoralBase.so
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/Boost/1.55.0_python2.7-dcbb8/x86_64-slc6-gcc48-dbg/lib/libboost_system-gcc48-mt-d-1_55.so.1.55.0
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/Boost/1.55.0_python2.7-dcbb8/x86_64-slc6-gcc48-dbg/lib/libboost_thread-gcc48-mt-d-1_55.so.1.55.0
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/gcc/4.8.4/x86_64-slc6/lib64/libstdc++.so.6.0.19
--xxxxx-- Reading syms from /lib64/libm-2.12.so
--xxxxx-- Reading syms from /cvmfs/sft.cern.ch/lcg/releases/gcc/4.8.4/x86_64-slc6/lib64/libgcc_s.so.1
--xxxxx-- Reading syms from /lib64/libpthread-2.12.so
--xxxxx-- Reading syms from /lib64/libc-2.12.so
--xxxxx-- Reading syms from /lib64/librt-2.12.so
--xxxxx-- REDIR: 0x3b5c084d70 (libc.so.6:strcasecmp) redirected to 0x4801696 (_vgnU_ifunc_wrapper)
--xxxxx-- REDIR: 0x3b5c087030 (libc.so.6:strncasecmp) redirected to 0x4801696 (_vgnU_ifunc_wrapper)
--xxxxx-- REDIR: 0x3b5c081200 (libc.so.6:__GI_strlen) redirected to 0x4a09a90 (__GI_strlen)
--xxxxx-- REDIR: 0x3b5c082ce0 (libc.so.6:__GI_strrchr) redirected to 0x4a093f0 (__GI_strrchr)
--xxxxx-- REDIR: 0x3b5c07f780 (libc.so.6:strcmp) redirected to 0x4801696 (_vgnU_ifunc_wrapper)
--xxxxx-- REDIR: 0x3b5c128450 (libc.so.6:__strcmp_sse42) redirected to 0x4a0ac30 (__strcmp_sse42)
--xxxxx-- REDIR: 0x60e62b0 (libstdc++.so.6:operator new(unsigned long)) redirected to 0x4a06f4e (operator new(unsigned long))
--xxxxx-- REDIR: 0x3b5c081430 (libc.so.6:__GI_strncmp) redirected to 0x4a0a2c0 (__GI_strncmp)
--xxxxx-- REDIR: 0x3b5c07a320 (libc.so.6:calloc) redirected to 0x4a087ae (calloc)
--xxxxx-- REDIR: 0x3b5c0811c0 (libc.so.6:strlen) redirected to 0x4801696 (_vgnU_ifunc_wrapper)
--xxxxx-- REDIR: 0x3b5c133720 (libc.so.6:__strlen_sse42) redirected to 0x4a09af0 (__strlen_sse42)
--xxxxx-- REDIR: 0x3b5c089710 (libc.so.6:memcpy) redirected to 0x4a0b330 (memcpy)
--xxxxx-- REDIR: 0xffffffffff600400 (???:???) redirected to 0x3806bd5d (vgPlain_amd64_linux_REDIR_FOR_vtime)
--xxxxx-- REDIR: 0x3b5c083470 (libc.so.6:bcmp) redirected to 0x4801696 (_vgnU_ifunc_wrapper)
--xxxxx-- REDIR: 0x3b5c13e7d0 (libc.so.6:__memcmp_sse4_1) redirected to 0x4a0c590 (__memcmp_sse4_1)
--xxxxx-- REDIR: 0x60e4570 (libstdc++.so.6:operator delete(void*)) redirected to 0x4a07f7a (operator delete(void*))
--xxxxx-- REDIR: 0x3b5c0838a0 (libc.so.6:memmove) redirected to 0x4a0cd90 (memmove)
--xxxxx-- REDIR: 0x3b5c0845a0 (libc.so.6:mempcpy) redirected to 0x4a0d460 (mempcpy)
--xxxxx-- REDIR: 0x3b5c083a40 (libc.so.6:memset) redirected to 0x4801696 (_vgnU_ifunc_wrapper)
--xxxxx-- REDIR: 0x3b5c083a80 (libc.so.6:__GI_memset) redirected to 0x4a0cce0 (memset)
--xxxxx-- REDIR: 0x3b5c07a6e0 (libc.so.6:malloc) redirected to 0x4a06a46 (malloc)
--xxxxx-- REDIR: 0x3b5c07f700 (libc.so.6:__GI_strchr) redirected to 0x4a09520 (__GI_strchr)
--xxxxx-- REDIR: 0x3b5c08ea20 (libc.so.6:__GI_strstr) redirected to 0x4a0d9c0 (__strstr_sse2)
--xxxxx-- REDIR: 0x3b5c0833f0 (libc.so.6:memchr) redirected to 0x4a0ad20 (memchr)
--xxxxx-- REDIR: 0x3b5c08de80 (libc.so.6:__GI_strncpy) redirected to 0x4a09e60 (__GI_strncpy)
--xxxxx-- REDIR: 0x3b5c07b5c0 (libc.so.6:free) redirected to 0x4a07b60 (free)
--xxxxx-- REDIR: 0x3b5c07f7c0 (libc.so.6:__GI_strcmp) redirected to 0x4a0ab90 (__GI_strcmp)
--xxxxx-- REDIR: 0x3b5c0834a0 (libc.so.6:__GI_memcmp) redirected to 0x4a0c510 (__GI_memcmp)
--xxxxx-- REDIR: 0x3b5c08ab90 (libc.so.6:strchrnul) redirected to 0x4a0d340 (strchrnul)
--xxxxx-- REDIR: 0xffffffffff600000 (???:???) redirected to 0x3806bd53 (vgPlain_amd64_linux_REDIR_FOR_vgettimeofday)
--xxxxx-- REDIR: 0x60e63c0 (libstdc++.so.6:operator new[](unsigned long)) redirected to 0x4a075f3 (operator new[](unsigned long))
--xxxxx-- REDIR: 0x60e45a0 (libstdc++.so.6:operator delete[](void*)) redirected to 0x4a0842a (operator delete[](void*))
==xxxxx== 
==xxxxx== HEAP SUMMARY:
==xxxxx==     in use at exit: 0 bytes in 0 blocks
==xxxxx==   total heap usage: 4,845 allocs, 4,845 frees, 1,471,139 bytes allocated
==xxxxx== 
==xxxxx== All heap blocks were freed -- no leaks are possible
==xxxxx== 
==xxxxx== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 5 from 5)
--xxxxx-- 
--xxxxx-- used_suppression:      5 dl-hack3-cond-1 /cvmfs/sft.cern.ch/lcg/releases/LCG_78root6/valgrind/3.10.0/x86_64-slc6-gcc48-dbg/lib/valgrind/default.supp:1206
==xxxxx== 
==xxxxx== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 5 from 5)
